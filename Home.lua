Home = {}
Home.name = "Home"
Home.slashCmd = "/home"

function Home.SlashCommand()
  local houseId = GetHousingPrimaryHouse()
  if houseId and CanLeaveCurrentLocationViaTeleport() then
      RequestJumpToHouse(houseId)
  else
    d("Could not teleport to primary residence.")
  end
end

function Home:Initialize()
  SLASH_COMMANDS[Home.slashCmd] = Home.SlashCommand
end

function Home.OnAddOnLoaded(event, addonName)
  if addonName == Home.name then
    Home:Initialize()
    EVENT_MANAGER:UnregisterForEvent(Home.addonName, EVENT_ADD_ON_LOADED)
  end
end

EVENT_MANAGER:RegisterForEvent(Home.name, EVENT_ADD_ON_LOADED, Home.OnAddOnLoaded)
